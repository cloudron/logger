(function () {
'use strict';

function init() {
    if (!localStorage.accessToken) {
        app.ready = true;
        return;
    }

    app.busy = true;

    superagent.get('/api/profile').query({ access_token: localStorage.accessToken }).end(function (error, result) {
        if (error) {
            delete localStorage.accessToken;
            app.busy = false;
            app.ready = true;
            return;
        }

        app.session.username = result.body.username;
        app.session.valid = true;
        app.ready = true;

        getSources(function () { hashChange(); });
    });
}

function login() {
    app.loginData.busy = true;

    superagent.post('/api/login').send({ username: app.loginData.username, password: app.loginData.password }).end(function (error, result) {
        app.loginData.busy = false;

        if (error) return console.error(error);
        if (result.statusCode === 401) return console.error('Invalid credentials');

        localStorage.accessToken = result.body.accessToken;

        init();
    });
}

function logout() {
    superagent.post('/api/logout').query({ access_token: localStorage.accessToken }).end(function (error) {
        if (error) console.error(error);

        app.session.valid = false;

        delete localStorage.accessToken;
    });
}

function addSource() {
    var name = 'Source ' + (app.sources.length+1);

    superagent.post('/api/sources').query({ access_token: localStorage.accessToken }).send({ name: name }).end(function (error) {
        if (error) return console.error(error);

        getSources();
    });
}

function showSources() {
    app.view = 'sources';
    app.sourceActive = {};
    app.logs = [];
}

function showLogs(sourceId) {
    var source = app.sources.find(function (s) { return s._id === sourceId; });
    if (!source) return showSources();

    $('.ui.search').search({
        apiSettings: {
            url: '/api/logs?search={query}&access_token=' + source.token,
            onResponse: function (result) {
                result.logs.forEach(function (l) {
                    l.prettyDate = moment(l.timestamp).format('MMM DD hh:mm:ss');
                });
                return { results: result.logs };
            }
        },
        fields: {
            title: 'prettyDate',
            description: 'content'
        },
        onSelect: function (result, response) {
            fetchLogs(result.timestamp);
            return false;
        }
    });

    app.view = 'logs';
    app.sourceActive = source;

    window.location.hash = '#' + source._id;

    fetchLogs();
}

function postprocessLogs(l) {
    l.richContent = window.ansiToHTML(l.content);
}

function fetchLogs(timestamp) {
    app.fetching = true;

    var query = {
        access_token: app.sourceActive.token
    };

    if (timestamp) {
        query.timestamp = timestamp;
        query.older = true;
    }

    superagent.get('/api/logs').query(query).end(function (error, result) {
        if (error) return console.error(error);

        result.body.logs.forEach(postprocessLogs);

        app.logs = result.body.logs;
        app.fetching = false;

        window.setTimeout(function () { document.getElementById('scrollBottom').scrollIntoView(true); }, 500);
    });
}

function fetchNewerLogs () {
    if (app.fetching) return;
    app.fetching = true;

    var newestLog = app.logs[app.logs.length-1];

    superagent.get('/api/logs').query({ access_token: app.sourceActive.token, timestamp: newestLog.timestamp }).end(function (error, result) {
        if (error) return console.error(error);

        result.body.logs.forEach(postprocessLogs);

        app.logs = app.logs.concat(result.body.logs);
        app.fetching = false;
    });
}

function fetchOlderLogs () {
    if (app.fetching) return;
    app.fetching = true;

    var oldestLog = app.logs[0];

    superagent.get('/api/logs').query({ access_token: app.sourceActive.token, timestamp: oldestLog.timestamp, older: true }).end(function (error, result) {
        if (error) return console.error(error);

        result.body.logs.forEach(postprocessLogs);

        app.logs = result.body.logs.concat(app.logs);
        app.fetching = false;
    });
}

function getSources(callback) {
    callback = callback || function (error) { if (error) console.error(error); };

    app.busy = true;

    superagent.get('/api/sources').query({ access_token: localStorage.accessToken }).end(function (error, result) {
        if (error) return callback(error);

        app.sources = result.body.sources;
        app.busy = false;

        callback(null);
    });
}

function copyToClipboard(id, event) {
    var copyTextarea = document.getElementById(id);
    copyTextarea.select();

    try {
        var successful = document.execCommand('copy');
        if (successful) {
            $(event.target).popup({ on: 'manual', position: 'top center' });
            $(event.target).popup('show');
            setTimeout(function () { $(event.target).popup('hide'); }, 2000);
        }
    } catch (err) {
        console.log('Oops, unable to copy');
    }
}

function modalSourceEditShow(source) {
    app.sourceActive = source;
    app.sourceEdit.busy = false;
    app.sourceEdit.busyDelete = false;
    app.sourceEdit.busyPurge = false;
    app.sourceEdit.id = source._id;
    app.sourceEdit.name = source.name;
    app.sourceEdit.description = source.description;
    app.sourceEdit.retentionPick = '';
    app.sourceEdit.retentionHours = Math.round(source.retention / (60 * 60));

    $('#modalSourceEdit').modal('show');

    $('#modalSourceEdit .checkbox').checkbox({
        onChecked: function () {
            app.sourceEdit.retentionPick = $(this).val();
        }
    });

    $('#modalSourceEditDeleteButton').popup({
        on: 'click',
        hoverable: true
    });

    switch (app.sourceEdit.retentionHours) {
        case 0: $('#modalSourceEdit .checkbox [value="forever"]').parent().checkbox('check'); app.sourceEdit.retentionPick = 'forever'; break;
        case 1: $('#modalSourceEdit .checkbox [value="hour"]').parent().checkbox('check'); app.sourceEdit.retentionPick = 'hour'; break;
        case 24: $('#modalSourceEdit .checkbox [value="day"]').parent().checkbox('check'); app.sourceEdit.retentionPick = 'day'; break;
        case (24*7): $('#modalSourceEdit .checkbox [value="week"]').parent().checkbox('check'); app.sourceEdit.retentionPick = 'week'; break;
        default: $('#modalSourceEdit .checkbox [value="custom"]').parent().checkbox('check'); app.sourceEdit.retentionPick = 'custom'; break;
    }
}

function modalSourceEditSave() {
    app.sourceEdit.busy = true;

    var retentionHours;
    switch (app.sourceEdit.retentionPick) {
        case 'custom': retentionHours = app.sourceEdit.retentionHours; break;
        case 'forever': retentionHours = 0; break;
        case 'hour': retentionHours = 1; break;
        case 'day': retentionHours = 24; break;
        case 'week': retentionHours = 24 * 7; break;
        default: retentionHours = null;
    }

    var data = {
        name: app.sourceEdit.name,
        description: app.sourceEdit.description,
        retention: retentionHours * 60 * 60
    };

    superagent.put('/api/sources/' + app.sourceActive._id).query({ access_token: localStorage.accessToken }).send(data).end(function (error) {
        app.sourceEdit.busy = false;
        if (error) return console.error(error);

        app.sourceActive = {};
        app.sourceEdit = {};

        getSources();

        $('#modalSourceEdit').modal('hide');
    });
}

function modalSourceEditDelete() {
    app.sourceEdit.busyDelete = true;

    superagent.del('/api/sources/' + app.sourceActive._id).query({ access_token: localStorage.accessToken }).end(function (error) {
        app.sourceEdit.busyDelete = false;
        if (error) return console.error(error);

        app.sourceActive = {};
        app.sourceEdit = {};

        getSources();

        $('#modalSourceEdit').modal('hide');
    });
}

function modalSourceEditPurge() {
    app.sourceEdit.busyPurge = true;

    superagent.del('/api/logs').query({ access_token: app.sourceActive.token }).end(function (error) {
        app.sourceEdit.busyPurge = false;
        if (error) return console.error(error);
    });
}

Vue.filter('prettyDate', function (value) {
    return moment.utc(value).format('MMM DD HH:mm:ss');
});

var app = new Vue({
    el: '#app',
    data: {
        ready: false,
        busy: false,
        session: {
            valid: false
        },
        loginData: {
            busy: false
        },
        sources: [],
        logs: [],
        fetching: false,
        sourceActive: {},
        sourceEdit: {
            id: '',
            name: '',
            description: '',
            retentionPick: '',
            retentionHours: 0,
            token: '',
            busy: false,
            busyDelete: false,
            busyPurge: false
        },
        view: 'sources'
    },
    methods: {
        login: login,
        logout: logout,
        showSources: showSources,
        showLogs: showLogs,
        fetchLogs: fetchLogs,
        addSource: addSource,
        copyToClipboard: copyToClipboard,
        modalSourceEditShow: modalSourceEditShow,
        modalSourceEditSave: modalSourceEditSave,
        modalSourceEditDelete: modalSourceEditDelete,
        modalSourceEditPurge: modalSourceEditPurge
    }
});

window.app = app;

function hashChange() {
    if (!app.session.valid) return;

    if (window.location.hash.slice(1)) showLogs(window.location.hash.slice(1));
    else showSources();
}

$(window).bind('hashchange', hashChange);

init();

var logContainer = $('.logs');
logContainer.on('wheel', function (event) {
    if (app.fetching) return;

    if (event.originalEvent.deltaY < 0 && logContainer[0].scrollTop === 0) return fetchOlderLogs();
    if (event.originalEvent.deltaY > 0 && logContainer[0].scrollHeight ===  logContainer[0].scrollTop + logContainer[0].clientHeight) return fetchNewerLogs();
});

})();
