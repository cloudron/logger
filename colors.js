'use strict';

var Convert = require('ansi-to-html');
var convert = new Convert();

global.ansiToHTML = function (str) {
    return convert.toHtml(str);
};
