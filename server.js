#!/usr/bin/env node

'use strict';

require('supererror');

var express = require('express'),
    morgan = require('morgan'),
    passport = require('passport'),
    compression = require('compression'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    lastMile = require('connect-lastmile'),
    auth = require('./backend/auth.js'),
    routes = require('./backend/routes.js'),
    database = require('./backend/database.js');

var app = express();
var router = new express.Router();

router.post  ('/api/login', auth.login);
router.post  ('/api/logout', auth.verify, auth.logout);
router.get   ('/api/profile', auth.verify, auth.getProfile);
router.get   ('/api/healthcheck', function (req, res) { res.status(200).send(); });

router.post  ('/api/sources', auth.verify, routes.sources.add);
router.get   ('/api/sources', auth.verify, routes.sources.get);
router.put   ('/api/sources/:sourceId', auth.verify, routes.sources.edit);
router.delete('/api/sources/:sourceId', auth.verify, routes.sources.del);

router.post  ('/api/logs', routes.sources.verify, routes.logs.add);
router.get   ('/api/logs', routes.sources.verify, routes.logs.get);
router.delete('/api/logs', routes.sources.verify, routes.logs.del);

app.use(morgan('dev'));
app.use(compression());
app.use('/api', bodyParser.json());
app.use('/api', bodyParser.urlencoded({ extended: false, limit: '100mb' }));
app.use('/api', cookieParser());
app.use('/api', session({ secret: 'logging logging', resave: false, saveUninitialized: false }));
app.use('/api', passport.initialize());
app.use('/api', passport.session());
app.use(router);
app.use('/', express.static(__dirname + '/frontend'));
app.use(lastMile());

database.init(function (error) {
    if (error) {
        console.error(error);
        process.exit(1);
    }

    var server = app.listen(3000, function () {
        var host = server.address().address;
        var port = server.address().port;

        console.log('Logger listening at http://%s:%s', host, port);
    });
});
