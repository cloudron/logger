/* jslint node:true */

'use strict';

exports = module.exports = {
    init: init,
    clear: clear,

    sources: {
        add: sourcesAdd,
        get: sourcesGet,
        getByAccessToken: sourcesGetByAccessToken,
        edit: sourcesEdit,
        del: sourcesDel
    },

    logs: {
        get: logsGet,
        add: logsAdd,
        del: logsDel,
        cleanup: logsCleanup
    }
};

var assert = require('assert'),
    uuid = require('uuid/v4'),
    mongodb = require('mongodb');

var MONGODB_URL = process.env.MONGODB_URL || 'mongodb://127.0.0.1:27017/logger';
var DEFAULT_LOG_RETENTION = 60 * 60 * 24;     // default log retention in seconds - 7 days
var TIMESTAMP_INDEX_NAME = 'timestamp_1';

var gDatabase, gSources = null;
var gLogs = {};

function init(callback) {
    assert.strictEqual(typeof callback, 'function');

    mongodb.MongoClient.connect(MONGODB_URL, function (error, db) {
        if (error) return callback(error);

        gDatabase = db;

        gDatabase.createCollection('sources');
        gSources = gDatabase.collection('sources');

        callback();
    });
}

function getLogsCollection(sourceId) {
    assert.strictEqual(typeof sourceId, 'string');

    if (!gLogs[sourceId]) {
        gDatabase.createCollection('logs_' + sourceId);
        gLogs[sourceId] = gDatabase.collection('logs_' + sourceId);

        // ensure indexes
        gLogs[sourceId].createIndex({ content: 'text' });
        gLogs[sourceId].createIndex({ sourceId: 1 });
        rebuildTimestampIndex(sourceId);
    }

    return gLogs[sourceId];
}

function rebuildTimestampIndex(sourceId) {
    assert.strictEqual(typeof sourceId, 'string');

    sourcesGet(function (error, sources) {
        if (error) return console.error(error);

        var source = sources.find(function (s) { return s._id.toString() === sourceId; });
        if (!source) return console.error('No such source', sourceId);

        getLogsCollection(sourceId).indexes(function (error, indexes) {
            if (error) return console.error(error);

            var index = indexes.find(function (i) { return i.name === TIMESTAMP_INDEX_NAME; });

            // check if retention has changed
            if (index && index.expireAfterSeconds === source.retention) return;

            // rebuild index with new rentention
            getLogsCollection(sourceId).dropIndex(TIMESTAMP_INDEX_NAME, function (error) {
                if (error && error.code !== 27) console.error(error);   // ignore not found errors

                getLogsCollection(sourceId).createIndex({ timestamp: 1 }, { expireAfterSeconds: source.retention }, function (error) {
                    if (error) console.error(error);
                });
            });
        });
    });
}

function clear(callback) {
    assert.strictEqual(typeof callback, 'function');

    mongodb.MongoClient.connect(MONGODB_URL, function (error, db) {
        if (error) return callback(error);

        db.dropDatabase(callback);
    });
}

function sourcesAdd(name, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof callback, 'function');

    var doc = {
        name: name,
        retention: DEFAULT_LOG_RETENTION,
        token: uuid()
    };

    gSources.insert(doc, function (error, result) {
        if (error) return callback(error);
        if (!result) return callback(new Error('no result returned'));

        callback(null, result);
    });
}

function sourcesGet(callback) {
    assert.strictEqual(typeof callback, 'function');

    gSources.find({}).toArray(function (error, result) {
        if (error) return callback(error);

        callback(null, result);
    });
}

function sourcesGetByAccessToken(accessToken, callback) {
    assert.strictEqual(typeof accessToken, 'string');
    assert.strictEqual(typeof callback, 'function');

    gSources.find({ token: accessToken }).toArray(function (error, result) {
        if (error) return callback(error);
        if (!result[0]) return callback(new Error('not found'));

        callback(null, result[0]);
    });
}

function sourcesEdit(sourceId, name, description, retention, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof description, 'string');
    assert.strictEqual(typeof retention, 'number');
    assert.strictEqual(typeof callback, 'function');

    var data = {
        name: name,
        retention: retention,
        description: description,
    };

    gSources.update({_id: new mongodb.ObjectId(sourceId) }, { $set: data }, function (error) {
        if (error) return callback(error);
        callback();

        // now rebuild the timestamp index if needed
        rebuildTimestampIndex(sourceId);
    });
}

function sourcesDel(sourceId, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert.strictEqual(typeof callback, 'function');

    gSources.deleteMany({ _id: new mongodb.ObjectId(sourceId) }, function (error) {
        if (error) return callback(error);

        getLogsCollection(sourceId).drop(function (error) {
            if (error) return callback(error);

            delete gLogs[sourceId];

            callback();
        });
    });
}

function logsAdd(sourceId, timestamp, content, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert.strictEqual(typeof timestamp, 'object');
    assert.strictEqual(typeof content, 'string');
    assert.strictEqual(typeof callback, 'function');

    var doc = {
        sourceId: sourceId,
        timestamp: timestamp,
        content: content,
    };

    getLogsCollection(sourceId).insert(doc, function (error, result) {
        if (error) return callback(error);
        if (!result) return callback(new Error('no result returned'));

        callback();
    });
}

function logsGet(sourceId, query, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert(Array.isArray(query));
    assert.strictEqual(typeof callback, 'function');

    query.push({ sourceId: sourceId });

    getLogsCollection(sourceId).find({ $and: query }).sort({ timestamp: -1 }).limit(100).toArray(function (error, result) {
        if (error) return callback(error);

        callback(null, result.reverse());
    });
}

function logsDel(sourceId, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert.strictEqual(typeof callback, 'function');

    getLogsCollection(sourceId).deleteMany({ sourceId: sourceId }, function (error, result) {
        if (error) return callback(error);
        callback(null, result.deletedCount);
    });
}

function logsCleanup(sourceId, timestamp, callback) {
    assert.strictEqual(typeof sourceId, 'string');
    assert.strictEqual(typeof timestamp, 'number');
    assert.strictEqual(typeof callback, 'function');

    var query = {
        $and: [
            { sourceId: sourceId },
            { timestamp: { $lt: timestamp }}
        ]
    };

    getLogsCollection(sourceId).deleteMany(query, function (error, result) {
        if (error) return callback(error);
        callback(null, result.deletedCount);
    });
}
