/* jslint node:true */

'use strict';

exports = module.exports = {
    sources: {
        verify: sourcesVerify,
        add: sourcesAdd,
        get: sourcesGet,
        edit: sourcesEdit,
        del: sourcesDel
    },
    logs: {
        get: logsGet,
        add: logsAdd,
        del: logsDel
    }
};

var assert = require('assert'),
    async = require('async'),
    moment = require('moment'),
    database = require('./database.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function sourcesVerify(req, res, next) {
    assert.strictEqual(typeof req.query, 'object');

    if (!req.query.access_token) return next(new HttpError(400, 'missing access_token'));
    if (typeof req.query.access_token !== 'string') return next(new HttpError(400, 'access_token must be a string'));

    database.sources.getByAccessToken(req.query.access_token, function (error, result) {
        if (error) return next(new HttpError(401, 'invalid token'));

        req.sourceId = result._id.toString();

        next();
    });
}

function sourcesAdd(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.name) return next(new HttpError(400, 'missing name'));
    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));

    database.sources.add(req.body.name, function (error, result) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(201, { source: result }));
    });
}

function sourcesGet(req, res, next) {
    database.sources.get(function (error, result) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(200, { sources: result }));
    });
}

function sourcesEdit(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.params.sourceId, 'string');

    if (!req.body.name) return next(new HttpError(400, 'missing name'));
    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));

    req.body.description = req.body.description || '';
    if (typeof req.body.description !== 'string') return next(new HttpError(400, 'description must be a string'));

    req.body.retention = req.body.retention;
    if (typeof req.body.retention !== 'number') return next(new HttpError(400, 'retention must be a number'));

    database.sources.edit(req.params.sourceId, req.body.name, req.body.description, req.body.retention, function (error) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(202, {}));
    });
}

function sourcesDel(req, res, next) {
    assert.strictEqual(typeof req.params.sourceId, 'string');

    database.sources.del(req.params.sourceId, function (error) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(202, {}));
    });
}

function logsGet(req, res, next) {
    assert.strictEqual(typeof req.sourceId, 'string');

    var query = [];

    if (req.query && req.query.search) {
        query.push({ $text: { $search: req.query.search }});
    }

    if (req.query && req.query.timestamp) {
        if (req.query.older) {
            query.push({ timestamp: { $lt: moment.utc(parseInt(req.query.timestamp, 10)).toDate() }});
        } else {
            query.push({ timestamp: { $gt: moment.utc(parseInt(req.query.timestamp, 10)).toDate() }});
        }
    } else {
        query.push({ timestamp: { $lt: moment.utc().toDate() }});
    }

    database.logs.get(req.sourceId, query, function (error, result) {
        if (error) return next(new HttpError(500, error));

        result.forEach(function (log) {
            log.timestamp = moment(log.timestamp).utc().valueOf();
        });

        next(new HttpSuccess(200, { logs: result }));
    });
}

function logsAdd(req, res, next) {
    assert.strictEqual(typeof req.sourceId, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.logs) return next(new HttpError(400, 'missing logs'));
    if (!Array.isArray(req.body.logs)) return next(new HttpError(400, 'logs must be an array'));

    var valid = req.body.logs.every(function (l) {
        return (moment.utc(l.timestamp).isValid() && (typeof l.content === 'string'));
    });

    if (!valid) return next(new HttpError(400, 'ensure all logs entries have a valid timestamp and content'));

    async.each(req.body.logs, function (l, callback) {
        l.timestamp = moment.utc(l.timestamp).toDate();
        database.logs.add(req.sourceId, l.timestamp, l.content, callback);
    }, function (error) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(201, {}));
    });
}

function logsDel(req, res, next) {
    assert.strictEqual(typeof req.sourceId, 'string');

    database.logs.del(req.sourceId, function (error) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(202, {}));
    });
}
